import { useEffect, useState, createContext } from "react";

export const AuthContext = createContext<any>(null);

export const AuthContextProvider = ({ children }: any) => {
  const [user, setUser] = useState(
    JSON.parse(localStorage.getItem("user")!) || null
  );

  const login = () => {
    setUser({ id: 1, name: "Name", avatar: "https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png" });
  };

  useEffect(() => {
    localStorage.setItem("user", JSON.stringify(user));
  }, [user]);

  return (
    <AuthContext.Provider value={{ user, login }}>{children}</AuthContext.Provider>
  );
};
