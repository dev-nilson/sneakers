import "./Profile.scss";

function Profile() {
  return (
    <div className="profile">
      <div className="images">
        <img
          className="cover"
          src="https://reactplay.io/static/media/placeholder_cover.ea7b18e0704561775829.jpg"
          alt="cover"
        />
        <img
          className="profile"
          src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png"
          alt="profile"
        />
      </div>
      <div className="container">
        <h1>Name</h1>
        <button>Follow</button>
      </div>
    </div>
  );
}

export default Profile;
