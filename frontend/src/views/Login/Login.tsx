import { useContext } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "../../context/authContext";
import "./Login.scss";

function Login() {
  const { login } = useContext(AuthContext);

  return (
    <div className="login">
      <div className="login__container">
        <div className="login__left">
          <h1>sneakloset</h1>
          <p>Lorem ipsum</p>
          <Link to="/register">
            <button>Register</button>
          </Link>
        </div>
        <div className="login__right">
          <h1>Login</h1>
          <form>
            <input type="text" placeholder="Username" />
            <input type="password" placeholder="Password" />
            <button onClick={login}>Login</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Login;
