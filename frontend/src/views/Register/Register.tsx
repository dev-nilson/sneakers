import { Link } from "react-router-dom";
import "./Register.scss";

function Register() {
  return (
    <div className="register">
      <div className="register__container">
        <div className="register__left">
          <h1>sneakloset</h1>
          <p>Lorem ipsum</p>
          <Link to="/login">
            <button>Login</button>
          </Link>
        </div>
        <div className="register__right">
          <h1>Register</h1>
          <form>
            <input type="email" placeholder="Email" />
            <input type="text" placeholder="Username" />
            <input type="password" placeholder="Password" />
            <button>Register</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Register;
