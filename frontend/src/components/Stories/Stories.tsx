import "./Stories.scss";

function Stories() {
  const stories = [
    {
      id: 1,
      name: "User1",
      img: "https://eapp.org/wp-content/uploads/2018/05/poster_placeholder.jpg"
    },
    {
      id: 2,
      name: "User2",
      img: "https://eapp.org/wp-content/uploads/2018/05/poster_placeholder.jpg"
    },
    {
      id: 3,
      name: "User3",
      img: "https://eapp.org/wp-content/uploads/2018/05/poster_placeholder.jpg"
    },
  ]

  return (
    <div className="stories">
      {stories.map(story => (
        <div className="story" key={story.id}>
          <img src={story.img} />
          <span>{story.name}</span>
        </div>
      ))}
    </div>
  );
}

export default Stories;
