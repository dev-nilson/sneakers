import ExploreIcon from "@mui/icons-material/Explore";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import SellIcon from "@mui/icons-material/Sell";
import InfoIcon from "@mui/icons-material/Info";
import SettingsIcon from "@mui/icons-material/Settings";
import "./Sidebar.scss";

function Sidebar() {
  return (
    <div className="sidebar">
      <div className="container">
        <div className="menu">
          <div className="item">
            <ExploreIcon />
            <span>Explore</span>
          </div>
          <div className="item">
            <ShoppingCartIcon />
            <span>Buy</span>
          </div>
          <div className="item">
            <SellIcon />
            <span>Explore</span>
          </div>
        </div>
        <hr />
        <div className="menu">
          <span>Others</span>
          <div className="item">
            <InfoIcon />
            <span>About</span>
          </div>
          <div className="item">
            <SettingsIcon />
            <span>Settings</span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
