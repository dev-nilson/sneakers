import { useContext } from "react";
import { ThemeContext } from "../../context/themeContext";
import { Link } from "react-router-dom";
import SearchIcon from "@mui/icons-material/Search";
import PersonIcon from "@mui/icons-material/Person";
import LightModeIcon from "@mui/icons-material/LightMode";
import DarkModeIcon from '@mui/icons-material/DarkMode';
import "./Navbar.scss";

function Navbar() {
  const { darkMode, toggle } = useContext(ThemeContext);

  return (
    <div className="navbar">
      <div className="navbar__left">
        <Link to="/" className="link">
          <span>sneakloset</span>
        </Link>
        {darkMode ? (
          <LightModeIcon className="icon" onClick={toggle} />
        ) : (
          <DarkModeIcon className="icon" onClick={toggle} />
        )}

        <div className="search">
          <SearchIcon />
          <input type="text" placeholder="Search" />
        </div>
      </div>
      <div className="navbar__right">
        <PersonIcon />
      </div>
    </div>
  );
}

export default Navbar;
