import { useContext } from "react";
import { ThemeContext } from "../../context/themeContext";
import Navbar from "../Navbar/Navbar";
import Sidebar from "../Sidebar/Sidebar";
import Widgets from "../Widgets/Widgets";
import { Outlet } from "react-router-dom";
import "../../style.scss";

function Layout() {
  const { darkMode } = useContext(ThemeContext);

  return (
    <div className={`theme-${darkMode ? "dark" : "light"}`}>
      <Navbar />
      <div style={{ display: "flex" }}>
        <Sidebar />
        <div style={{ flex: 6 }}>
          <Outlet />
        </div>
        <Widgets />
      </div>
    </div>
  );
}

export default Layout;
