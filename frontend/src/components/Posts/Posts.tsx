import Post from "../Post/Post";
import "./Posts.scss";

function Posts() {
  const posts = [
    {
      id: 1,
      userId: "1",
      username: "Name1",
      avatar:
        "https://eapp.org/wp-content/uploads/2018/05/poster_placeholder.jpg",
      caption: "Caption",
      img: "https://eapp.org/wp-content/uploads/2018/05/poster_placeholder.jpg",
    },
    {
      id: 2,
      userId: "2",
      username: "Name2",
      avatar:
        "https://eapp.org/wp-content/uploads/2018/05/poster_placeholder.jpg",
      caption: "Caption",
      img: "https://eapp.org/wp-content/uploads/2018/05/poster_placeholder.jpg",
    },
    {
      id: 3,
      userId: "3",
      username: "Name3",
      avatar:
        "https://eapp.org/wp-content/uploads/2018/05/poster_placeholder.jpg",
      caption: "Caption",
      img: "https://eapp.org/wp-content/uploads/2018/05/poster_placeholder.jpg",
    },
  ];

  return (
    <div className="posts">
      {posts.map((post) => (
        <Post key={post.id} post={post} />
      ))}
    </div>
  );
}

export default Posts;
