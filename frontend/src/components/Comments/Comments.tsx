import { useContext } from "react";
import { AuthContext } from "../../context/authContext";
import "./Comments.scss";

function Comments() {
  const { user } = useContext(AuthContext);

  const comments = [
    {
      id: 1,
      userId: 1,
      name: "Name1",
      avatar:
        "https://www.pathwaysvermont.org/wp-content/uploads/2017/03/avatar-placeholder-e1490629554738.png",
      comment: "Comment1",
    },
    {
      id: 2,
      userId: 3,
      name: "Name2",
      avatar:
        "https://www.pathwaysvermont.org/wp-content/uploads/2017/03/avatar-placeholder-e1490629554738.png",
      comment: "Comment2",
    },
  ];

  return (
    <div className="comments">
      <div className="write">
        <img src={user.avatar} alt="" />
        <input type="text" placeholder="Write a comment..." />
        <button>Send</button>
      </div>
      {comments.map((comment) => (
        <div className="comment">
          <img src={comment.avatar} alt="" />
          <div className="info">
            <span>{comment.name}</span>
            <p>{comment.comment}</p>
          </div>
          <span className="date">1 hour ago</span>
        </div>
      ))}
    </div>
  );
}

export default Comments;
