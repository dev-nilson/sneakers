import { useState } from "react";
import { Link } from "react-router-dom";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import FavoriteIcon from "@mui/icons-material/Favorite";
import SmsIcon from "@mui/icons-material/Sms";
import Comments from "../Comments/Comments";
import "./Post.scss";

interface IPost {
  id: number;
  userId: string;
  username: string;
  avatar: string;
  caption: string;
  img: string;
}

type PostProps = {
  post: IPost;
};

function Post({ post }: PostProps) {
  const [areCommentsOpen, setAreCommentsOpen] = useState(false);

  const liked = false;

  return (
    <div className="post">
      <div className="container">
        <div className="user">
          <div className="userInfo">
            <img src={post.avatar} alt="user profile" />
            <div className="details">
              <Link to={`/profile/${post.userId}`} className="link">
                <span className="username">{post.username}</span>
              </Link>
              <span className="date">1 minute ago</span>
            </div>
          </div>
          <MoreHorizIcon />
        </div>
        <div className="content">
          <p>{post.caption}</p>
          <img src={post.img} alt="post" />
        </div>
        <div className="info">
          <div className="item">
            {liked ? <FavoriteIcon /> : <FavoriteBorderIcon />} 12 likes
          </div>
          <div
            className="item"
            onClick={() => setAreCommentsOpen(!areCommentsOpen)}
          >
            <SmsIcon /> 10 comments
          </div>
        </div>
        {areCommentsOpen && <Comments />}
      </div>
    </div>
  );
}

export default Post;
